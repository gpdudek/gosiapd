import React from 'react'
import PropTypes from 'prop-types'
import clsx from 'clsx'

import style from './button.module.scss'

export default function Button({ text, className, href, onClick, big, ...otherprops }) {
  return href ? (
    <a
      href={href}
      rel="noopener noreferrer"
      className={clsx(big ? style.bigButton : style.smallButton, className)}
      {...otherprops}
    >
      {text}
    </a>
  ) : (
    <button
      className={clsx(big ? style.bigButton : style.smallButton, className)}
      onClick={onClick}
      {...otherprops}
    >
      {text}
    </button>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
}
