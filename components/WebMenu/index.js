import React from 'react'
import Link from 'next/link'

import Pill from '../../components/Pill'
import ButtonIcon from '../../components/ButtonIcon'
import clsx from 'clsx'

import { projects } from '../../databases/projects.js'

import style from './webmenu.module.scss'

export default function WebMenu({ className }) {
  return (
    <div className={clsx(style.buttonsContainer, className)}>
      <p className={style.description}>Please select one of the projects below ⤵</p>
      {projects.map((project, id) => {
        return (
          <div className={style.buttonContainer}>
            <ButtonIcon
              src={`/icons/${project.icon}.png`}
              href={`/web/${project.adress}`}
              className={style.buttonIconCard}
            />
            <Link href={`/web/${project.adress}#content`}>
              <a className={style.card}>
                <div>
                  <h3>{project.companyName} &rarr;</h3>
                  <p>{project.role}</p>
                  <div className={style.pills}>
                    {project.skills.map((skill, index) => {
                      return <Pill text={skill} />
                    })}
                  </div>
                </div>
              </a>
            </Link>
          </div>
        )
      })}
    </div>
  )
}
