import React from 'react'
import clsx from 'clsx'

import ButtonIcon from '../../components/ButtonIcon'

import style from './socialMedia.module.scss'

export default function SocialMedia({ className }) {
  return (
    <div className={clsx(className)}>
      <ButtonIcon
        src="/icons/linkedin.png"
        href="https://www.linkedin.com/in/gosia-palys-dudek/"
        className={style.buttonIcon}
      />
      {/* <ButtonIcon
        src="/icons/github.png"
        href="https://github.com/mpdudek"
        className={style.buttonIcon}
      /> */}
      <ButtonIcon
        src="/icons/gitlab.png"
        href="https://gitlab.com/gpdudek"
        className={style.buttonIcon}
      />
      <ButtonIcon
        src="/icons/dots.png"
        href="https://the-dots.com/users/gosia-palys-dudek-145777"
        className={style.buttonIcon}
      />
    </div>
  )
}
