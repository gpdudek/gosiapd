import React from 'react'
import clsx from 'clsx'

import style from './buttonIcon.module.scss'

export default function ButtonIcon({ src, href, alt, className, ...otherprops }) {
  return (
    <a href={href} target="_blank">
      <img className={clsx(style.container, className)} src={src} alt={alt} {...otherprops} />
    </a>
  )
}
