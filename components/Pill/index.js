import React from 'react'
import clsx from 'clsx'

import style from './pill.module.scss'

export default function Pill({ text, className, ...otherprops }) {
  return (
    <div className={clsx(style.container, className)} {...otherprops}>
      {text}
    </div>
  )
}
