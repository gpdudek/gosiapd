import React from 'react'
import Link from 'next/link'

import ButtonIcon from '../ButtonIcon'

import style from './navbar.module.scss'

export default function Navbar() {
  return (
    <nav className={style.container}>
      <ul>
        <li className={style.logo}>
          <Link href="/">Frontend Developer</Link>
        </li>
        <li>
          <ul className={style.icons}>
            <li className={style.icon}>
              <ButtonIcon src="/icons/www (1).png" href="/web" />
              <p>My Best Website</p>
            </li>
            <li className={style.icon}>
              <ButtonIcon src="/icons/iphone.png" href="/mobile" />
              <p>My Best Mobile App</p>
            </li>
            <li className={style.icon}>
              <ButtonIcon src="/icons/pen.svg" href="/cv" />
              <p>about me</p>
            </li>
          </ul>
        </li>
        {/* <li className={style.socialTable}>
          <ButtonIcon
            src="/icons/LinkedIn.svg"
            href="https://www.linkedin.com/in/gosia-palys-dudek/"
          />
          <ButtonIcon src="/icons/fb.svg" href="https://www.facebook.com/gosiapalys" />
          <ButtonIcon
            src="/icons/Instagram.svg"
            href="https://www.instagram.com/malgosiapalysdudek/"
          />
          <ButtonIcon src="/icons/Github.svg" href="https://github.com/mpdudek" />
          <ButtonIcon src="/icons/Gitlab.svg" href="https://gitlab.com/gpdudek" />
          <ButtonIcon src="/icons/mail.svg" href="mailto:gosia@pnd.io" />
          <ButtonIcon
            src="/icons/favicon.png"
            href="https://the-dots.com/users/gosia-palys-dudek-145777"
          />
          <ButtonIcon src="/icons/apple(2).svg" href="/cv" />
          <ButtonIcon src="/icons/twitter.svg" href="https://twitter.com/gosiapd" />
        </li> */}
      </ul>
    </nav>
  )
}
