import Reacat from 'react'

import style from './footer.module.scss'

export default function Footer() {
  return (
    <footer className={style.footer}>
      <p>
        UX/UI built by <a href="https://www.kasiapalys.com/">kasiapalys.com</a> | Built by me
      </p>
    </footer>
  )
}
