import React from 'react'

export default function Pictures() {
    constructor() {
      super()
      this.state = {
        fullPhoto: null,
      }
    }
  
    setFullPhoto = (picture) => {
      this.setState({
        fullPhoto: picture,
      })
    }
  
    render() {
      const { data, version, marginTop, photo } = this.props
      const { fullPhoto } = this.state
  
      let display = null
  
      switch (version) {
        case 'ribbon':
          display = (
            <div className={style.picturRibbon}>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </div>
          )
          break
  
        case 'one':
          display = (
            <PictureOne>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </PictureOne>
          )
          break
  
        case 'three':
          display = (
            <PictureThree>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </PictureThree>
          )
          break
  
        case 'five':
          display = (
            <PictureFive>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </PictureFive>
          )
          break
  
        case 'eight':
          display = (
            <PictureEight>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </PictureEight>
          )
          break
  
        default:
          display = (
            <PictureSix>
              {data.map((picture, index) => (
                <LiView key={index}>
                  <img
                    src={picture.src}
                    alt="indoor view"
                    onClick={() => this.setFullPhoto(picture)}
                  />
                </LiView>
              ))}
            </PictureSix>
          )
      }
  
      return (
        <Container marginTop={marginTop}>
          <BigPhoto active={fullPhoto ? true : false}>
            {fullPhoto && <img src={fullPhoto.src} />}
            <CrossIcon onClick={() => this.setFullPhoto(null)}>
              <CrossOneLine />
              <CrossTwoLine />
            </CrossIcon>
          </BigPhoto>
          {display}
        </Container>
      )
    }
  }
  