import React from 'react'

import Button from '../Button'
import ButtonIcon from '../ButtonIcon'

import { projects } from '../../databases/projects.js'

import style from './mobileMenu.module.scss'

export default function MobileMenu() {
  return (
    <div className={style.container}>
      <p className={style.description}>Please select one of the project below ⤵</p>
      {projects.map((project, id) => {
        return (
          <div>
            <div>
              <div className={style.titleWraper}>
                <ButtonIcon
                  src={`/icons/${project.icon}.png`}
                  href={`/web/${project.adress}`}
                  className={style.buttonIconCard}
                />

                <div className={style.companyNameWraper}>
                  <h3 className={style.companyName}>{project.companyName}</h3>
                </div>
              </div>
              <p className={style.role}>{project.role}</p>
            </div>
            <div className={style.buttonWrap}>
              <Button
                text="MORE"
                href={`/web/${project.adress}#content`}
                className={style.buttonMore}
              />
            </div>
          </div>
        )
      })}
    </div>
  )
}
