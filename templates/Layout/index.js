import React from 'react'
import clsx from 'clsx'
import Link from 'next/link'

import Footer from '../../components/Footer'
import Pill from '../../components/Pill'
import Button from '../../components/Button'
import WebMenu from '../../components/WebMenu'
import SocialMedia from '../../components/SocialMedia'

import style from './layout.module.scss'

export default function Layout({ children, className }) {
  return (
    <div className={clsx(style.container, className)}>
      <div className={style.wrapperIDmobile}>
        <h1 className={style.title}>Gosia Palys-Dudek</h1>
        <h3 className={style.mobiletitle}>frontend developer 🧞‍♀️</h3>
        <h2 className={style.subtitle}>Let me make your idea clickable!</h2>
      </div>
      <div className={style.topskillsContainer}>
        <div className={style.skillsContainer}>
          <h2 className={style.titleSkills}>Main skills</h2>
          <div className={style.skillsWrap}>
            <Pill text="React" />
            <Pill text="Next.js ❤️" />
            <Pill text="Redux" />
            <Pill text="Java Script" />
            <Pill text="React Native" />
            <Pill text="SASS" />
            <Pill text="CSS Modules" />
            <Pill text="Open Minded" />
            <Pill text="Action Oriented" />
          </div>
        </div>
        <Button big text="Say Hello" href="mailto:gosia@pnd.io" className={style.buttonMail} />
      </div>
      <div className={style.heroContainer}>
        <Link href={`/`} className={style.wrapperIDWeb}>
          <a>
            <h1 className={style.title}>Gosia Palys-Dudek</h1>
            <h2 className={style.subtitle}>Let me make your idea clickable!🧞‍♀️</h2>
          </a>
        </Link>

        <div className={style.content}>
          <Link href={`/`}>
            <a>
              <img
                src="/images/ja/komp2.jpg"
                alt="coding in the living room"
                className={style.heroImage}
              />
            </a>
          </Link>
          <div>
            <SocialMedia className={style.socialMediaMobile} />

            <p className={style.description}>
              I love creating something out of nothing 🧞‍♀️, turning vision into code 👩🏻‍💻. Changing a
              static design into a living product, a rich website or a mobile app . An idea is worth
              100x more when it's turned into a product.
            </p>
            <p className={style.description}>
              Giving you possibility to experience your ideas, to have opportunity to share them
              with others makes me happy and is the goal of my professional life.
            </p>
            <div className={style.buttonMailWraper}>
              <Button
                big
                text="Say Hello"
                href="mailto:gosia@pnd.io"
                className={style.buttonMailMobile}
              />
            </div>

            <SocialMedia className={style.socialMediaWeb} />
          </div>
        </div>
      </div>
      <h2 className={style.subtitle}>Below are some of the projects I was involved in.</h2>
      <div id="content" className={style.projectContainerWeb}>
        <WebMenu className={style.webMenuWraper} />
        <div>{children}</div>
      </div>
      <Footer />
    </div>
  )
}
