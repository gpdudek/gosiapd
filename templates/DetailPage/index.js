import React from 'react'

import style from './detailpage.module.scss'

export default function DetailPage({ children }) {
  return <div className={style.main}>{children}</div>
}
