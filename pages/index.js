import React from 'react'
import Head from 'next/head'

import Layout from '../templates/Layout'
import MobileMenu from '../components/MobileMenu'
import WebMenu from '../components/WebMenu'

import useWindowDimensions from '../hooks/useWindowDimensions'

import TheDots from './web/the-dots'

export default function Home() {
  const { width } = useWindowDimensions()
  if (width <= 768) {
    return (
      <Layout>
        <Head>
          <title>GosiaPD</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <MobileMenu />
      </Layout>
    )
  } else if (width <= 1200) {
    return (
      <Layout>
        <Head>
          <title>GosiaPD</title>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <WebMenu />
      </Layout>
    )
  } else {
    return <TheDots />
  }
}
