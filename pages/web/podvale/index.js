import React from 'react'

import Layout from '../../../templates/Layout'

import Pill from '../../../components/Pill'
import Button from '../../../components/Button'
import ButtonIcon from '../../../components/ButtonIcon'

import { projects } from '../../../databases/projects.js'

import style from './podvale.module.scss'

export default function Podvale() {
  const projectsPodvale = projects.find((project) => project.id === '3')
  return (
    <Layout>
      <div className={style.container}>
        <div className={style.containerMobile}>
          <h3 className={style.company}>Podvale </h3>
          <a className={style.webLink} href="https://podvale.pnd.io/" target="_blank">
            podvale.pnd.io
          </a>
          <ButtonIcon
            src="/icons/gitlab.png"
            href="https://gitlab.com/gpdudek/podvale"
            className={style.buttonIcon}
          />
          <p className={style.text}>
            A website created as an advertisement for an apartment, targeted at people who adhered
            to the principles of cosiness and well-being of Hygge and Logam.
          </p>
          <div className={style.mobile}>
            {projectsPodvale.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>

          <img src="/images/podvale/pod1.jpeg" className={style.img}></img>
          <img src="/images/podvale/pod2.jpeg" className={style.img}></img>
          <div className={style.textMobileWraper}>
            <p className={style.text}>
              The website's design focuses on typography and photos. The font shape and layout in
              the photos show style and character. Kasia Palys was responsible for the UX / UI page
              of it.
            </p>
            <Button
              text="kasiapalys.com"
              href="https://www.kasiapalys.com/"
              className={style.pillButton}
            />
          </div>
        </div>

        <div className={style.secondColumn}>
          <div className={style.web}>
            {projectsPodvale.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>
          <p className={style.text}>
            I used styled-components library where you can style your components with plain CSS
            inside your javascript code.
          </p>
          <img src="/images/podvale/pod3.jpeg" className={style.img}></img>
          <img src="/images/podvale/pod4.jpeg" className={style.img}></img>
          <img src="/images/podvale/pod5.jpeg" className={style.img}></img>
        </div>
      </div>

      <div className={style.buttonsContainer}>
        <Button
          text="Back"
          href="/web/michalnesterowicz#content"
          className={style.buttonNavigation}
        />
        <Button text="Next" href="/web/focusson#content" className={style.buttonNavigation} />
      </div>
      <div className={style.buttonMailWraper}>
        <Button text="Say Hello" href="mailto:gosia@pnd.io" />
      </div>
    </Layout>
  )
}
