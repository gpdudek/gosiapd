import React from 'react'
import Head from 'next/head'

import Pill from '../../../components/Pill'
import Layout from '../../../templates/Layout'
import Button from '../../../components/Button'
import ButtonIcon from '../../../components/ButtonIcon'

import { projects } from '../../../databases/projects.js'

import style from './mobile.module.scss'

export default function TheDotsMobile() {
  const projectsMobile = projects.find((project) => project.id === '1')
  return (
    <div>
      <Head>
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@tim_cook" />
        <meta name="twitter:title" content="https://t.co/82FtXyuMiI" />
        <meta name="twitter:description" content="https://platform.twitter.com/widgets.js" />
        <meta
          name="twitter:image"
          content="https://twitter.com/tim_cook/status/1154390961612414978?ref_src=twsrc%5Etfw"
        />
      </Head>

      <Layout className={style.webContainer}>
        <div className={style.container}>
          <div>
            <h3 className={style.company}>The-Dots mobile app</h3>
            <a className={style.webLink} href="https://the-dots.com" target="_blank">
              the-dots.com
            </a>
            <p className={style.text}>
              I was the second developer to build this application from scratch.
            </p>
            <div className={style.mobile}>
              {projectsMobile.skills.map((skill) => {
                return <Pill text={skill} className={style.pill} />
              })}
            </div>
            <p className={style.text}>
              The Dots is a network of Makers, Doers, Fixers and Dreamers — collectively creating a
              better future. Forbes asked "Is this the next LinkedIn?"
            </p>
            <p className={style.text}>
              We have done a fantastic job in almost unrealistically short time 🎄🎁🎅🏾, that I am
              proud and happy of. Later on, the company decided to rebuild it with native Swift for
              iOS 😩 for business reasons.
            </p>
            <img src="/images/the-dots/app.jpg" className={style.img} />
            <p className={style.text}>
              Shortly after the launch of the app it was features by Apple as App of the Day! ❤️ And
              it made our day! Later on, because of this success we were invited to be part of the
              second cohort of Apple Entrepreneurship Camp and visited Apple Campus to master our
              skills and improve our mobile app 👩🏻‍💻😍🙌🏼 Do you see me on the pic? 👇🏼
            </p>
            <h4 className={style.importentInfo}>
              I WAS ON THE
              <br />
              <b>APPLE CAMP</b>
            </h4>
            <blockquote class="twitter-tweet" hide_thread="true" data-conversation="none">
              <p lang="en" dir="ltr">
                Inspired by the work of these remarkable entrepreneurs and developers!{' '}
                <a href="https://t.co/82FtXyuMiI">https://t.co/82FtXyuMiI</a>
              </p>
              &mdash; Tim Cook (@tim_cook){' '}
              <a href="https://twitter.com/tim_cook/status/1154390961612414978?ref_src=twsrc%5Etfw">
                July 25, 2019
              </a>
            </blockquote>{' '}
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            <p className={style.text}>
              Two weeks at the Apple Camp gave me a great opportunity to learn from the experience
              of people who are creating probably one of the strongest brands all over the world. To
              see how they work, how they get inspiration had impacted the rest of my professional
              work.
            </p>
            <img src="/images/the-dots/bages.png" />
          </div>

          <div className={style.secondColumn}>
            <div className={style.web}>
              {projectsMobile.skills.map((skill) => {
                return <Pill text={skill} className={style.pill} />
              })}
            </div>
            <div className={style.textButtonWraper}>
              <p className={style.text}>
                Please learn more details about this trip on the project on The-Dots 'The Dots App @
                Apple Park' written by Pip Jamieson - Founder of The Dots
              </p>
              <Button
                text="The Dots App @ Apple Park"
                href="https://the-dots.com/projects/the-dots-app-at-apple-park-336817"
                className={style.pillButton}
              />
            </div>
            <img src="/images/the-dots-mobile/app1.jpeg" className={style.img} />
            <div className={style.textButtonWraper}>
              <p className={style.text}>
                Please read more about the process of building the app from Daniel Harvey - Head of
                Product Design & Brand.
              </p>
              <Button
                text="Designing The Dots App"
                href="https://the-dots.com/projects/designing-the-dots-ios-app-209405"
                className={style.pillButton}
              />
            </div>
            <img src="/images/the-dots-mobile/app2.jpeg" className={style.img} />
            <img src="/images/the-dots-mobile/app3.jpeg" className={style.img} />
            <img src="/images/the-dots-mobile/USA.jpg" className={style.img} />
            <img src="/images/the-dots-mobile/SanFan.jpeg" className={style.img} />
          </div>
        </div>

        <div className={style.buttonsContainer}>
          <Button text="Back" href="/web/the-dots#content" className={style.buttonNavigation} />
          <Button
            text="Next"
            href="/web/michalnesterowicz#content"
            className={style.buttonNavigation}
          />
        </div>

        <div className={style.buttonMailWraper}>
          <Button text="Say Hello" href="mailto:gosia@pnd.io" className={style.buttonMail} />
        </div>
      </Layout>
    </div>
  )
}
