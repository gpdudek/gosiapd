import React from 'react'
import Head from 'next/head'

import Pill from '../../../components/Pill'
import Layout from '../../../templates/Layout'
import Button from '../../../components/Button'
import ButtonIcon from '../../../components/ButtonIcon'

import { projects } from '../../../databases/projects.js'

import style from './thedots.module.scss'

export default function TheDots() {
  const projectsTheDots = projects.find((project) => project.id === '0')

  return (
    <div>
      <Head>
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@tim_cook" />
        <meta name="twitter:title" content="https://t.co/82FtXyuMiI" />
        <meta name="twitter:description" content="https://platform.twitter.com/widgets.js" />
        <meta
          name="twitter:image"
          content="https://twitter.com/tim_cook/status/1154390961612414978?ref_src=twsrc%5Etfw"
        />
      </Head>

      <Layout>
        <div className={style.container}>
          <div className={style.containerMobile}>
            <h3 className={style.company}>The-Dots</h3>
            <a className={style.webLink} href="https://the-dots.com" target="_blank">
              the-dots.com
            </a>
            <p className={style.text}>
              I was part of the team for 3 years creating this great platform.
            </p>
            <div className={style.mobile}>
              {projectsTheDots.skills.map((skill) => {
                return <Pill text={skill} className={style.pill} />
              })}
            </div>
            <p className={style.text}>
              The Dots is a network of Makers, Doers, Fixers and Dreamers — collectively creating a
              better future. Forbes asked "Is this the next LinkedIn?"
            </p>
            <div className={style.mobile}>
              <p className={style.text}>You can find my profile on the </p>
              <ButtonIcon
                src="/icons/dots.png"
                href="https://the-dots.com/users/gosia-palys-dudek-145777"
                className={style.pillButton}
              />
            </div>
            <p className={style.text}>
              I was honored to be part of the second cohort of Apple Entrepreneurship Camp that
              visited Apple Campus to master our skills and improve our mobile app 👩🏻‍💻😍🙌🏼 Do you see
              me on the pic? 👇🏼
            </p>
            <h4 className={style.importentInfo}>
              I WAS AT THE
              <br />
              <b>APPLE CAMP</b>
            </h4>
            <blockquote class="twitter-tweet">
              <p lang="en" dir="ltr">
                Inspired by the work of these remarkable entrepreneurs and developers!{' '}
                <a href="https://t.co/82FtXyuMiI">https://t.co/82FtXyuMiI</a>
              </p>
              &mdash; Tim Cook (@tim_cook){' '}
              <a href="https://twitter.com/tim_cook/status/1154390961612414978?ref_src=twsrc%5Etfw">
                July 25, 2019
              </a>
            </blockquote>{' '}
            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            <p className={style.text}>
              Two weeks at the Apple Camp gave me a great opportunity to learn from the experience
              of people who are creating probably one of the strongest brands all over the world. To
              see how they work, how they get inspiration had impacted the rest of my professional
              work.
            </p>
            <img src="/images/the-dots/bages.png" />
          </div>

          <div className={style.secondColumn}>
            <div className={style.web}>
              {projectsTheDots.skills.map((skill) => {
                return <Pill text={skill} className={style.pill} />
              })}
            </div>
            <div className={style.web}>
              <p className={style.text}>You can find my profile on The-Dots </p>
              <ButtonIcon
                src="/icons/dots.png"
                href="https://the-dots.com/users/gosia-palys-dudek-145777"
                className={style.pillButton}
              />
            </div>
            <p className={style.text}>
              I joined the company during their extensive development of 2.0 version. Initially as
              an intern, I stayed to become the Frontend Developer and second developer on the first
              version of the mobile app.
            </p>
            <img src="/images/the-dots/page3.jpeg" className={style.img} />
            <p className={style.text}>
              During my time there the social network grew from 100,000 to 500,000 members ❤️. The
              engineering team built an amazing infrastructure to support this growth and at the
              same time deliver new advanced features to foster community diversity like bias
              blocker (to make unconscious bias in hiring a thing of the past), which I fully built
              👩🏻‍💻🧑🏽‍💻👨🏿‍💻👩🏾‍💻👩🏻‍💻🧑🏾‍💻.
            </p>
            <img src="/images/the-dots/page4.jpeg" className={style.img} />
            <img src="/images/the-dots/page5.png" className={style.img} />
            <img src="/images/the-dots/InnternnationalWD.jpg" className={style.img} />
          </div>
        </div>

        <div className={style.buttonsContainer}>
          <Button text="Back" href="/web/focusson#content" className={style.buttonNavigation} />
          <Button
            text="Next"
            href="/web/the-dots-mobile#content"
            className={style.buttonNavigation}
          />
        </div>

        <div className={style.buttonMailWraper}>
          <Button text="Say Hello" href="mailto:gosia@pnd.io" className={style.buttonMail} />
        </div>
      </Layout>
    </div>
  )
}
