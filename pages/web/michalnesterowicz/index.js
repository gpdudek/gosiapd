import React from 'react'

import Layout from '../../../templates/Layout'

import Pill from '../../../components/Pill'
import Button from '../../../components/Button'
import ButtonIcon from '../../../components/ButtonIcon'

import { projects } from '../../../databases/projects.js'

import style from './michalnesterowicz.module.scss'

export default function MichalNesterowicz() {
  const projectsMN = projects.find((project) => project.id === '2')
  return (
    <Layout>
      <div className={style.container}>
        <div>
          <h3 className={style.company}>Michal N.</h3>
          <a className={style.webLink} href="https://www.michalnesterowicz.com/" target="_blank">
            michalnesterowicz.com
          </a>
          <div className={style.assetsIconsWraper}>
            <ButtonIcon
              src="/icons/gitlab.png"
              href="https://gitlab.com/gpdudhttps://gitlab.com/gpdudek/michalnesterowicz"
              className={style.buttonIcon}
            />
            <ButtonIcon
              src="/icons/dots.png"
              href="https://the-dots.com/projects/michalnesterowicz-com-213793"
              className={style.pillButton}
            />
          </div>

          <p className={style.text}>
            A business card page for a Polish symphony conductor - Michal Nesterowicz. Known and
            respected all over the world, the winner of the Cadaqués Orchestra European Conducting
            Competition in 2008 and among the prizewinners of the 6th Grzegorz Fitelborg
            International Conducting Competition in Katowice.
          </p>
          <div className={style.mobile}>
            {projectsMN.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>
          <div className={style.mobile}>
            <p className={style.text}>You can read more about the process of creation at </p>
            <Button
              text="About building website."
              href="https://the-dots.com/projects/michalnesterowicz-com-213793"
              className={style.pillButton}
            />
          </div>
          <img src="/images/michalnesterowicz/hero.jpg"></img>
          <p className={style.text}>In this case I was doing design and building the website.</p>
          <p className={style.text}>
            Before I started working on the website’s design I wanted to give myself some pragmatic
            criteria to understand his expectations and needs by which to judge the work. I needed
            to hear his story👂🏻👁, to learn his style of communication, taste and sense of humor. To
            know him well enough to get into his shoes 👞
          </p>
          <img src="/images/michalnesterowicz/newsAms.jpg" className={style.img}></img>
        </div>

        <div className={style.secondColumn}>
          <div className={style.web}>
            {projectsMN.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>
          <div className={style.web}>
            <p className={style.textReadMore}>Please read more about the process of creation at </p>
            <ButtonIcon
              src="/icons/dots.png"
              href="https://the-dots.com/projects/michalnesterowicz-com-213793"
              className={style.pillButton}
            />
          </div>
          <p className={style.text}>
            I decided to build this website in React because the priority was user-friendly CMS and
            flexible and quick possibility of redesigning or extending it. One important criteria
            was to have full control over files and a fast-functioning website. This was all
            provided by React.
          </p>
          <img src="/images/michalnesterowicz/newsSing.jpg" className={style.img}></img>
          <img src="/images/michalnesterowicz/contact.jpg" className={style.img}></img>
          <img src="/images/michalnesterowicz/photos.jpg" className={style.img}></img>
        </div>
      </div>
      <div className={style.buttonsContainer}>
        <Button
          text="Back"
          href="/web/the-dots-mobile#content"
          className={style.buttonNavigation}
        />
        <Button text="Next" href="/web/podvale#content" className={style.buttonNavigation} />
      </div>
      <div className={style.buttonMailWraper}>
        <Button text="Say Hello" href="mailto:gosia@pnd.io" />
      </div>
    </Layout>
  )
}
