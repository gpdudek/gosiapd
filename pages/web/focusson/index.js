import React from 'react'

import Pill from '../../../components/Pill'
import Layout from '../../../templates/Layout'
import Button from '../../../components/Button'
import ButtonIcon from '../../../components/ButtonIcon'

import { projects } from '../../../databases/projects.js'

import style from './focusson.module.scss'

export default function Focusson() {
  const projectsFocusson = projects.find((project) => project.id === '4')

  return (
    <Layout>
      <div className={style.container}>
        <div>
          <h3 className={style.company}>Focusson</h3>
          <a className={style.webLink} href="https://www.focusson.com/" target="_blank">
            focusson.com
          </a>
          <p className={style.text}>
            For almost 10 years I was co-founder and CEO of this wonderful product👩🏻‍💻☎️💅🏼👩🏻‍🎨.
          </p>
          <div className={style.mobile}>
            {projectsFocusson.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>
          <p className={style.text}>
            Focusson provides qualitative data analysis software and allows researchers to do focus
            group interviews.
          </p>
          <p className={style.text}>
            Below is a mini-questionnaire to be completed by respondents👇🏼
          </p>
          <img src="/images/focusson/Ankieta z komórki 4 copy.jpeg" className={style.img} />
          <p className={style.text}>
            Focusson was much more than just a great crazy run roller coaster machine🚆🏋🏼‍♂️, which
            allowed me to try myself as a co-founder and entrepreneur, give me massive knowledge
            about business and monetizing sides of it, but mainly it was a possibility to create a
            completely abstract idea. Creating a business from scratch required to think big and
            abstract thoughts, while at the same time be realistic about what can be achieved in
            limited time and with limitedd resoures. That was probably the biggest challenge.
          </p>
          <img src="/images/focusson/Ankieta z komórki 3 copy.jpeg" className={style.img} />
        </div>

        <div className={style.secondColumn}>
          <div className={style.web}>
            {projectsFocusson.skills.map((skill) => {
              return <Pill text={skill} className={style.pill} />
            })}
          </div>
          <img src="/images/focusson/Inter moderator survey 3 copy.png" className={style.img} />
          <p className={style.text}>
            Online tools allow for everything that a good reasearch requires. No one can be seen,
            everyone is anonymous👩🏻‍💻🧑🏾‍💻👩🏾‍💻👨🏿‍💻🧑🏽‍💻👀🗣. There is no reason to hide your thoughts. Do
            your customers really think your product is intuitive to use, or are they simply ashamed
            to admit that they struggle with it?
          </p>
          <img
            src="/images/focusson/coding system analysis without space copy.png"
            className={style.img}
          />
          <p className={style.text}>
            I was responsible for designing and creating online tools that allow quantitative
            analysis of qualitative data. Responsible for the content side of the platform, parts of
            frontend development and implementing research focus for testing. Cooperated with the
            biggest research centers in Poland, including PBS DGA, Educational Research Institute
            and other institutions like University of Wrocław or University of Social Sciences and
            Humanities.
          </p>
          <img src="/images/focusson/Inter res survey 6 copy.png" className={style.img} />
          <p className={style.text}>
            It was a great time, great team and people I really learned a lot from. A piece of my
            heart will always be the focusson ❤️ Love you guys!
          </p>
          <img src="/images/focusson/my.jpeg" className={style.img} />
        </div>
      </div>
      <div className={style.buttonsContainer}>
        <Button text="Back" href="/web/podvale#content" className={style.buttonNavigation} />
        <Button text="Next" href="/web/the-dots#content" className={style.buttonNavigation} />
      </div>
      <div className={style.buttonMailWraper}>
        <Button text="Say Hello" href="mailto:gosia@pnd.io" className={style.buttonMail} />
      </div>
    </Layout>
  )
}
